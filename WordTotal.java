import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;

public class WordTotal {

    public static class E_EMapper extends MapReduceBase implements Mapper<
                    LongWritable ,       /*Input key Type */
                    Text,                /*Input value Type*/
                    Text,                /*Output key Type*/
                    IntWritable>         /*Output value Type*/{

        
        public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) 
                            throws IOException {

            String line = value.toString();
            String lasttoken = null;
            StringTokenizer s = new StringTokenizer(line," ");
            int total = 0;
            while(s.hasMoreTokens()) {
                s.nextToken();
                total++;
            }
            output.collect(new Text("total"), new IntWritable(total));
        }

    }

    public static class E_EReduce extends MapReduceBase implements Reducer< Text, IntWritable, Text, IntWritable > {

        public void reduce(Text key, Iterator <IntWritable> values, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {

            int total = 0;

            while (values.hasNext()) {
                total += values.next().get();
            }
             output.collect(key, new IntWritable(total));
            
        }
    }

    public static void main(String args[])throws Exception {
        
        JobConf conf = new JobConf(WordTotal.class);

        conf.setJobName("wordtotal");
        conf.setOutputKeyClass(Text.class);
        conf.setOutputValueClass(IntWritable.class);
        conf.setMapperClass(E_EMapper.class);
        conf.setCombinerClass(E_EReduce.class);
        conf.setReducerClass(E_EReduce.class);
        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);

        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));

        JobClient.runJob(conf);
    }
} 